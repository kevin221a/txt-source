# novel

- title: 処刑少女の生きる道
- title_zh1: 处刑少女的生存之道
- author: 佐藤真登
- illust: ニリツ
- source: http://q.dmzj.com/2759/index.shtml
- cover: https://images-na.ssl-images-amazon.com/images/I/81qEZFqDeeL.jpg
- publisher: GA文庫
- date: 2019-12-09T18:09:22+08:00
- status: 连载中
- novel_status: 0x0100

## illusts


## publishers

- dmzj

## series

- name: 处刑少女的生存之道

## preface


```
――这是，她为了杀她的故事。
这个世界上，会有来自异世界日本的『迷途之人』。但是，由于过去迷途之人失控暴走，导致发生世界级的大灾害，因此只要看到他们就有必要杀掉。　
而在其中，处刑人梅诺，与迷途之人的少女灯里邂逅。梅诺虽然毫不犹豫且冷静彻底地执行任务。然而，本应确实被杀害的灯里，不知为何却平静复活了。无计可施的梅诺，为了寻找杀害不死之身的方法，欺骗灯里与她一起踏上旅途……
「梅诺酱～走吧！」
「……好好。我知道了啦。」
灯里很奇妙地，特别亲近梅诺，而这让梅诺开始心生些许动摇。
――这是，她为了杀她的故事。

これは、彼女が彼女を殺すための物語。

この世界には、異世界の日本から『迷い人』がやってくる。
だが、過去に迷い人の暴走が原因で世界的な大災害が起きたため、彼らは見つけ次第『処刑人』が殺す必要があった。
そんななか、処刑人のメノウは、迷い人の少女アカリと出会う。
躊躇なく冷徹に任務を遂行するメノウ。
しかし、確実に殺したはずのアカリは、なぜか平然と復活してしまう。
途方にくれたメノウは、不死身のアカリを殺しきる方法を探すため、彼女を騙してともに旅立つのだが……
「メノウちゃーん。行こ! 」
「……はいはい。わかったわよ」
妙に懐いてくるアカリを前に、メノウの心は少しずつ揺らぎはじめる。

GA文庫大賞、7年ぶりの《大賞》作品!
――これは、彼女が彼女を殺すための物語。
```

## tags

- node-novel
- dmzj
- 异界
- 日本
- 百合
- 穿越

# contribute

- 素素素
- 明明想休息却停不下来的s881116s
- 轻之国度
- 

# options

## dmzj

- novel_id: 2759

## downloadOptions

- noFilePadend: true
- filePrefixMode: 4
- startIndex: 1

## textlayout

- allow_lf2: true

# link

- https://mypage.syosetu.com/479700/
- https://twitter.com/qazxsw020119
- https://www.youtube.com/watch?v=XrtT-zBoPeY
- [处刑少女的生存之道吧](https://tieba.baidu.com/f?kw=%E5%A4%84%E5%88%91%E5%B0%91%E5%A5%B3%E7%9A%84%E7%94%9F%E5%AD%98%E4%B9%8B%E9%81%93&ie=utf-8 "处刑少女的生存之道")
- https://www.lightnovel.cn/thread-1006012-1-1.html
- 

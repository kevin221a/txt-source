六月第五周的領土戰裡，最終共有四個團隊進攻杉並戰區，分別是藍色軍團（獅子座流星雨）一隊、綠色軍團（長城）一隊、以豐島區為大本營的小規模軍團「Pondbag」與紅色軍團（日珥）各一隊。

由春雪、謠與晶組成的防守團隊，和Pondbag與日珥的進攻團隊對戰，兩場都贏得勝利。而黑雪公主與楓子的團隊儘管缺了一人，仍然對藍隊與綠隊大獲全勝。因此戰績是全領土都由防守方獲得百分之百的勝率，守住了黑暗星雲的旗幟。只是話說回來，自從去年十一月發出領土宣言以來，他們的領土就從來不曾淪陷過。

如果只看所屬人數，黑暗星雲會歸類在小軍團，卻能在領土戰中維持五成以上的勝率，當然是因為身為軍團長且有著壓倒性攻擊力的黑之王親自下場防守。雖說等級差距在BRAIN BURST里不構成絕對無法跨越的差距，但9級終究是完全不同的層次。她的外號「絕對切斷（World End）」可說名不虛傳。要想打倒四肢刀劍都能切斷任何物體的Black Lotus，唯一的方法就是先用非物理系拘束攻擊絆住她，再集中大量的遠程火力攻擊。但在攻方人數受限的領土戰裡，也很難湊出這樣的陣容。

因此，既然現階段其他諸王不親自進攻，有可能攻陷的也就只有不由黑雪公主防守的戰區。事實上，春雪+拓武+千百合的新生代團隊三人組，就有過不算少的敗績。但即使是二戰一勝或三戰兩勝，仍然能夠勉強維持五成的勝率，所以能有驚無險地守住旗幟。只是話說回來，即使黑雪公主不在，要是率領「四大元素」中多達兩人參加的團隊還打輸，責任就會歸屬到負責指揮的春雪頭上。

因為這樣的理由，當本周的領土戰時間結束，一回到梅鄉國中的後院，春雪立刻癱軟在椅子上。

「哈啊啊……還、還好沒打輸……」

聽到這句與勝利的歡呼相距甚遠的台詞，坐在旁邊的謠微微苦笑：

【UI＞辛苦了，有田學長。第二場對上Pondbag時，你就指揮得相當不錯。】

聊天視窗裡顯示的這行字乍看之下是讚美，卻也指出了第一場對上日珥的戰鬥就未能達到「相當不錯」的程度。春雪縮起脖子，試著對就讀國小四年級的老前輩辯解：

「如、如果至少事先告訴我要我當隊長，我還多少可以做點心理準備……」

【UI＞不管遇上任何狀況，都要能臨機應變！】

「是，你說得是……不過可倫姊回歸真是太好了，這樣一來在領土戰的團隊編組上，也會多出很多變化。」

春雪不經意地說出這句話，謠立刻可愛地嘟起嘴從下住上瞪著春雪。她就維持這樣的表情，雙手快速敲打投影鍵盤。

【UI＞有田學長是從什麼時候，知道今天倫姊會回黑暗星雲來的？】

被她這麼一逼問，事到如今也不能再打馬虎眼了。畢竟春雪在領土戰之前，就說過「高興的理由要保密」。

「是、是在前天傍晚……所以到現在也只過了四十六小時啊！我也嚇了一跳啊，那天亂鬥到一半，可倫姊突然登場救了我跟Ash兄……而、而且，四野宮學妹你剛剛才說遇上任何狀況都要臨機應變……

【UI＞那是指對戰中！】

謠先用力打出這行字，又忽然放鬆了表情。

她仰望春雪的一對大眼睛所反射出來的光線漸漸增加。從雲層縫隙間灑下的夕陽色光點，隨之化為水滴流過臉頰。

春雪瞪大眼睛，視野中慢慢跑過一串櫻花色的字串。

【UI＞相信這次一定也是因為有田學長在吧。】

「咦……這、這次？這話怎麼說？」

【UI＞不管是楓姊、我、還是幸幸，都是因為有田學長拚命努力，才能回到黑暗星雲。所以倫姊一定也一樣。】

「我、我根本什麼都沒做啊……反而是師父跟四野宮學妹，當然黑雪公主學姊也是，都是你們為了救我而回來……可倫姊也是一樣……」

【UI＞這你應該引以為傲才對。】

謠用右手擦了擦眼淚，注視指尖上搖曳的透明水珠。

這名小了春雪四歲的少女再度將視線移到他身上，將嘴唇張圓。

她的嘴角僵硬，頻頻顫抖。纖細的頸子冒出青筋，痙攣似的抽動。

「四、四野宮學妹……」

春雪以沙啞的聲音呼喊她。謠因為親眼目睹親生兄長，同時也是她超頻連線者「上輩」的四野宮竟也意外死亡，受到太大的打擊而失語。她能以肉聲說出的話，就只有靠長年練習之下，勉強練出的兩種BRAIN BURST相關語音指令。

失語症不是嘴的疾病，而是一種腦功能障礙。謠的症狀被分類為「下皮質運動性失語症」，對言語能夠正常理解與書寫，但進行自發言語──也就是用肉聲說話──就會有困難。主要的原因是大腦中一個叫做「中央溝前側腦回」的地方發生梗塞。而謠的情形則是過度的精神震撼，在該部位引發神經網路的障礙，即使用上大腦內建式晶片也無法恢復。

所以一旦謠想強行用肉聲說話，不只是肉體，連精神也會受到劇烈的痛楚。春雪舉起雙手想制止，但謠搶先一步從顫抖的嘴唇發出微弱但確切的聲音，沿著空氣送進春雪耳裡。

「謝……」

接著是「謝」，最後是「你」。

發完這三個音，謠已經額頭冒汗。但她仍然堅強地露出微笑，輕輕對春雪一鞠躬。

春雪拚命忍著幾乎就要奪眶而出的眼淚，輕聲說：

「……我才要說……謝謝你，四野宮學妹。你現在待在這裡……我真的很感激。」

謠聽了後抬起頭，露出滿臉她這年齡該有的天真笑容。

過了下午五點三十分後，春雪走過正門前尚未完成的校慶標門，與謠道別後獨自踏上歸途。

黑雪公主、拓武與千百合都尚未完成校慶準備。而且不只是他們，整間學校都籠罩在一股校慶前夕的熱力之中，獨自放學回家實在相當落寞。但過了強制放學時間的下午六點以後若還想留在校內，就非得由所屬的展示小組對校方管理部申請延長作業時間不可。春雪的二年C班早就提出作業完成的報告，所以當然不可能得到批准。

春雪踩著沉重的腳步來到青梅大道上，注視著道路對面的公車站牌。既然都被趕出學校，他是很想乾脆和昨天一樣跳上公車，一路遠征到中野戰區。但遺憾的是這種行動也遭到黑雪公主禁止。

原因是領土戰結束後進行的反省會上，春雪報告的紅色軍團旗下超頻連線者打破停戰歸定的理由。黑雪公主聽完事情經過之後。整整沉默了三秒鐘才忿忿地低聲說：「多半是那幫人幹的吧」。

春雪也想得到這句話指的是什麼人，但既然黑雪公主都說：「這件事全都由我來處理」，也就不敢說出名字。而且軍團長接著還當著眾人面前，幾乎就是針對春雪吩咐，要他們在校慶結束前于杉並區外不准進行自由對戰，甚至連觀戰都不行。

「……也還好啦，只要忍一天就過去了……」

校慶會在明天星期天下午結束，所以應該可以解釋成當天晚上就會解除領土外遠征的限制，應該是。到時候我一定要到中野跟Wolfram Cerberus再打一場，然後再對他說一次要他跟我來。

到了下周還會召開七王會議，只要會中承認春雪學會的「光學傳導」特殊能力有和「理論鏡面（Theoretical Mirror）」同樣的性能，針對東京中城大樓的連合進攻作戰就會開始推動。到時候春雪就要負責在攻略戰中打頭陣，面對「四神朱雀」以外，多半是他過去面對過的最強敵人──神獸級公敵「大天使梅丹佐」。春雪希望在這之前，能夠查清楚Cerberus那錯綜複雜又不為人知的糾葛，幫他一刀兩斷。

想救他的這種心意，說不定是一種不遜又傲慢的強迫推銷。因為如果單純以身為超頻連線者的戰鬥力來比較，5級的Silver Crow都未必能及1級的Cerberus。

但是前天傍晚，他以血肉之軀隔著人潮露臉時，他的眼神確實像在對春雪訴說些什麼。

既然如此，春雪就想回應他。不管幾次都想伸出援手，對他說話。就像過去許多這樣對待春雪的人一樣。

「……明天，我一定會去見你。」

春雪仰望中野方向染成紫色的積云輕聲自言自語說出這句話，就開始朝自己住的公寓大樓前進。

搭上大樓B棟的電梯，視野右上方就閃爍著收到郵件的圖示。春雪一邊打開郵件軟體，一邊想著多半是從昨天就到國外出差，最快要到明天晚上才會回來的母親寄來的。也許是想為沒辦法陪他去校慶而道歉吧。然而郵件上顯示的寄件人卻不是母親，只寫著一個字【N】。

「……這、這誰啊？」

春雪歪著頭叫出內文，上面也只寫著【我五秒鐘後去拜訪】。春雪越看越糊塗，把頭歪向另一邊，電梯正好就在這時停止，於是無意識地走出電梯。

結果沒過多久，旁邊的另一部電梯門也打開了。眷雪又以無意識的動作轉頭一看，就看到一個猛力跳到走廊上的人用右手手指朝春雪一指。

「喲，好久……也沒很久啊，大概五天不見？」

「啥……咦……咦咦咦！你為為為什麼會在這裡？」

春雪震驚過度，上半身後仰過了頭。好不容易恢復平衡姿勢就以傻眼的表情，對這個在他收到郵件剛好五秒鐘後準時出現的寄件人這麼說。

「我都先預告過了，何必嚇成這樣？而且你也不想想我都來這裡幾次了？」

「呃、呃……三、四……五……」

「只是隨口寒喧，不要真的去數好不好！」

這個把紅色頭髮綁在頭部兩側的女生，就是紅之王Scarlet Rain──上月由仁子，簡稱仁子。只見她大步走向春雪，輕輕拍了一下春雪的肚子，整張臉露出得意的笑容。她就這麼沿著公共走廊往有田家方向行進，春雪好不容易讓腦袋重開機，趕緊從後跟去。仁子搶先一步走到掛有2305門牌的門前，右手手指比劃幾下，立刻就聽到開鎖聲，讓春雪又嚇得倒退。

「咦、咦咦咦！你還有我家鑰匙喔？臨時通行碼的有效期限應該早就……」

「啊啊，上次我來你家過夜的時候，就對家用伺服器動了點手腳，改成永久通行碼了。」

仁子一邊輕描淡寫地說著駭人的台詞，一邊脫下運動鞋，說了聲：「打擾了～」就熟門熟路地走進去。接著又在走廊途中轉過身來，看著還在發呆的春雪。

「我自己會招呼自己，你先去換衣服沒關係。」

她丟下這句體貼的話，就消失在客廳中。

「……一般應該不是在五秒鐘前，是在五小時前預告吧？」

春雪無可奈何，只能這樣自言自語。

接著恭敬不如從命，先去把制服換成T恤與五分褲。回到客廳一看，仁子正躺在一人用的沙發上，頭靠在坐墊上。她光明正大把這裡當自己家的模樣，不由得讓春雪露出笑容，結果馬上就被瞪了一眼。

「你傻笑什麼？」

「什、什麼事都Nothing……喝、喝麥茶好嗎？」

「嗯，謝啦。」

春雪點點頭走向廚房，把麥茶倒進兩個玻璃杯端回來，就在仁子對面坐下。到了這個時候，他才自覺到自己從剛剛就覺得有點不對勁，又歪了好幾次頭，才總算想到理由，對還躺著的仁子問說：

「對了仁子，你今天為什麼從一開始就開正常模式？」

「咦？」

這個國小六年級女生頓時連連眨眼，立刻又露出甜笑說：

「怎麼？要我表演喔？」

她猛然坐起上身，正對春雪，雙手整整齊齊放在膝蓋上。不用先前那種「竊笑」的笑容而是換上滿臉笑嘻嘻的微笑說：

「……大哥哥也喜歡我對吧？我好高興！」

這天使模式在五天前的咖哩派對上並未發動，闊別許久之下再看到一次。春雪被這一記精神攻擊打個正著，上半身亂動了好一會兒才搖搖頭說：

「是，是不討厭啦。不，我不是說這個，只是想說是不是有什麼理由……」

話剛說到這裡，春雪才總算注意到自己已經知道這「理由」何在。不對，不只是仁子不開天使模式的理由，連她今天會這樣突然來訪的理由也都知道。

春雪大大地吸了口氣，停留在胸中一會兒後吐氣說道：

「這樣啊……說得也是啊。仁子會來這裡……想也知道是為了針對昨天無限制空間裡出的事來問我話……」

聽他這麼一說，仁子一邊把表情開回正常模式一邊慢慢讓身體往後倒，咚的一聲靠在椅背上。她利用坐墊的反作用力點點頭，有點憂郁地說：

「這也是有啦……算是理由的三分之一。」

「咦……那，剩下的三分之二呢？」

「這三分之二哩你明明也知道一半。是賠罪啦，我是來賠罪的。」

「賠罪……等等，是、是說你要道歉？」

「那還用說？不然跟你喝麥茶品什麼孤寂清幽啊？」

「是、是喔？原來仁子懂茶道？」

「你別小看我了，我可是曾經進過學校的茶道社呢！」

「曾經進過……那就是說已經不在了嘛……」

「少囉嗦！我都練到出師了啦……等等。」

仁子這時才注意到自己不知不覺間從沙發上探出上半身大聲嚷嚷，連眨了幾次眼睛，露出重重的苦笑。

「啊啊夠了，為什麼每次跟你說話都會搞成這樣，有夠容易被你影響的……趁還沒離題太遠，我要先跟你道歉！」

仁子斬釘截鐵地這麼宣告，就再度並攏纖細的雙腿，雙手啪的一聲拍在兩膝上放好，接著甩起用小小的黑色絲帶綁好的頭髮，深深一低頭：

「『日珥』的三名團員，在今天的領土戰裡打破停戰協定攻擊『黑暗星雲』的領土。我身為軍團長為此致歉！對不起！」

開場白的隆重與結尾的可愛感覺不太搭調，讓春雪不由得莞爾，趕緊綳緊表情，同時仁子也抬起了頭。正當春雪遲疑著不知道該怎麼回答，仁子又輕描淡寫地加上一句話。

「……這些幫我轉達給你們軍團那只黑色的。」

「咦？我、我來轉達？」

「那還用說！只對你這個基層戰鬥員道歉是能幹嘛？」

「基、基層……既既既然這樣，從一開始就不應該找我。直接去找學姊不就好了？」

「那有什麼辦法？我就只知道這裡啊。啊啊夠了，好啦，那我收回戰鬥員這句話，把你升格成怪人烏鴉男。」

「這、這算升格嗎？而且為什麼要採用邪惡組織的組織架構……」

春雪說到這裡，發現話題又離題到十萬八千里外，清了清嗓子後不再發言。他先放下自己想說的話點點頭回答：

「……知道了，我會把你剛剛的話轉達給黑之王。可是，不管是我、學姊，還是其他團員，都不覺得是跑來攻擊我們領土的Blaze Heart單方面有錯。他們是有動機非這麼做不可……所以如果可以，請你不要對Blaze他們施加太嚴厲的處罰……」

「我還想說你要講什麼，竟然先從這裡講起？」

仁子哼哼一笑，雙手放到後腦上輕輕點頭。

「雖然不能完全不罰，不過我是打算只罰他們下周獵公敵的出擊次數加倍就好。而且真要追究起來，我這個當日珥首領的又怎麼可以只講幾句話賠罪，什麼行動都不做就了事？」

「……行、行動？請問你的意思是？」

仁子先朝說話口氣莫名奇妙客氣起來的春雪瞥了一眼，再度端正姿勢，把握緊的雙手並攏在胸前展顏一笑：

「怎麼啦，大哥哥有什麼事想要我做？是打掃？洗衣服？還是……」

「不、不不不你什麼都不用做！而且都是仁子你從剛剛就這樣一直離題，話題才會越講越偏到奇怪的方向去！」

「奇怪的方向？是哪個方向？我只說要幫忙打理家事耶？我看是大哥哥想歪了吧？春雪大哥哥好色喔?」

仁子最後再把靦腆的微笑持續了兩秒左右，然後很乾脆地切換模式說下去：

「剛剛那段就當『有付諸行動的賠罪』可以吧？」

「嗚、嗚……你這樣一直切換，我頭部快暈了……」

「那我就再幫大哥哥加快腳步吧──?那Crow，我要說正事了。就是你一開始說的『在無限制空間出的事』……」

「嗚嗚嗚，好、好的。」

春雪雙手抱頭地點了點頭，已經完全變回紅之王表情的仁子以銳利的目光貫穿了他。

「Crow，你對事情真相心裡有底吧？」

「咦、真、真相……？」

「我說你喔，我也不覺得那只神獸級公敵背上的是真的Lotus好不好？雖然我只看了幾眼，可是資料壓就完全不一樣啊。」

仁子這句話，讓春雪好不容易重整好思緒，在腦內迅速整理過去得到的資訊。

根據Blaze Heart他們的說法，紅色軍團在昨天的星期五，出動了包括首領Scarlet Rain在內共有二十人以上的大集團，進入無限制空間獵殺公敵。地點是在豐島戰區……也就是池袋附近。結果突然有一隻背上載著一個虛擬角色的神獸級公敵出現，並對他們展開攻擊。公敵背上的虛擬角色立刻鑽進地面似的消失，仁子等人好不容易才從狂暴攻擊的公敵手下逃脫，成功地從傳送門脫身。

問題就在於公敵背上的虛擬角色，有著漆黑的裝甲和刀劍般銳利的四肢……

「……你的意思是說你看到的黑色虛擬角色，資料壓的強度……還超過黑雪公主學姊……？」

春雪戰戰兢兢地這麼一問，紅之王就搖了搖頭。

「正好相反，比Lotus弱得多了……而且即使由我來看，也幾乎看不到壓力。」

所謂「資料壓」，是仁子自己定出的度量衡。特過特殊能力「視覺擴張（Vision Extension）」，讓她甚至可以觀測出對戰虛擬角色所蘊含的戰鬥力與累積的戰鬥經驗多寡。她用這種能力，看出純色七王之中又以綠之王Green Grandee與藍之王Blue Knight的資料壓格外突出，並將這件事告知春雪。

但現在仁子的說法，卻讓春雪覺得意外。因為如果春雪所想像的「假Black Lotus」人選正確，他的資料壓應該有著直逼七王的規模。

「看、看不見……你的意思是說，這黑色虛擬角色是1、2級的新手？」

「……嗯──聽你這麼一說，又覺得不是這樣啊……該怎麼說，就好像明明在場卻又沒有實體……的那種感覺……」

仁子雙手抱胸沉吟了一會兒，忽然抬起頭來又瞪了春雪一眼：

「喂Crow！聽你的口氣，你果然知道些什麼吧！」

「呃、呃，呃呃……」

春雪有點退縮地天人交戰了幾秒鐘，最後死了心，點點頭說：

「嗯、嗯，我會說，我知道的事都會跟你說。可是……這要講很久很久，我們邊吃邊說吧……」

春雪正想問吃冷凍披薩可以嗎，仁子卻不讓他問完就搶先宣告：「咖哩！」看樣子她雖然對前幾天咖哩派對中登場的自制咖哩飯挑三揀四，其實卻相當地中意，但這個要求的難度實在太高了。

「仁、仁子，這也未免太強人所難了啦。那次的咖哩，實質上幾乎都是靠小百和四野宮做出來的。」

「我又沒叫你弄一樣的東西出來。只要隨便把材料切一切，再丟咖哩塊進去煮一煮，就差不多能吃了好不好……我也會幫忙的，春雪大哥哥，我們加油吧！」

──任務就在這樣的互動下開始，經過購物回合與烹調回合，好不容易完成像是咖哩的餐點時，客廳裡的類比時鐘指針已經超過了晚上七點。

當實在是別無選擇的微波冷凍白飯，以及兩大盤只有馬鈴薯、胡蘿蔔、洋蔥與雞肉作為配料的咖哩排上餐桌，春雪還是姑且問問看：

「我說仁子，你今天該不會……」

「我在這裡過夜。」

「這、這樣啊？」

──沒跟昨天學姊＆師父的過夜派對撞期真是太好了！

春雪心有戚戚焉地這麼想著，和仁子齊聲說了聲開動。拿起湯匙均衡地舀起咖哩與白飯，戰戰兢兢地送進嘴裡。

「……奇、奇怪……沒想到還挺……」

「……搞什麼？明明就很好吃嘛。」

兩人互相說出含有讚賞之意的印象，同時用力拿著湯匙往盤上舀去。雖然只是看了咖哩塊包裝上以AR顯示出來的參考烹調影片後，用大幅簡化過的方式煮出來，卻仍然有著咖哩該有的滋味，也不知道該說厲害還是理所當然。

春雪腦子裡轉著這樣的念頭轉了一會兒，這才想起半年前「第五代災禍之鎧事件」中仁子給他的禮物，於是一邊舀起第二匙咖哩一邊歪著頭說：

「可是奇怪了，記得仁子不是對烹飪很拿手嗎？」

「……你看看你湯匙上這塊我切的胡蘿蔔也該知道是怎樣！我跟你差不了多少啦，你是想挖苦我嗎？」

看到仁子從正常模式切換成紅之王模式，春雪趕緊否定：

「不、不不不是啦！因為之前仁子給我的餅乾真的好好吃……」

一聽到這句話，縱向的皺紋就從仁子的眉心消失，臉頰微微泛紅。

「不、不要用認真的表情講這種話啦……怎、怎麼說，甜點是另一回事啦。因為Pard的手藝是准職業級的，我跟她學了很多……」

「是喔！原來是這樣啊……」

身兼日珥副團長還扮演仁子監護者角色的Blood Leopard，簡稱Pard小姐在位於練馬區櫻台的一家蛋糕店打工。春雪一邊想起她穿著女僕風制服就騎上電動機車的英姿，一邊繼續說道：

「那Pard小姐不但負責櫃台，還會進廚房喔？好厲害啊，不知道她將來會不會走這條路。」

「啥？你說什麼鬼話？Pard是那家店……」

仁子說到這裡，卻莫名閉上嘴露出甜笑：

「算了，沒關係啦。總之我的廚藝技能是只限甜點類，而且只會做簡單的。」

「可是那些餅幹好酥脆，口感又很扎實，真的好好吃……」

「就跟你說不要再講這個啦！趁還熱的時候趕快吃一吃啦！」

仁子剛嚷嚷完就猛然開始動起湯匙，讓春雪不由得莞爾地看著她看了好一會兒，隨即自己也舀起滿滿一湯匙的雞肉咖哩，滿嘴嚼了起來。胡蘿蔔和馬鈴薯都切得很難看，而且有點煮太久，但春雪仍然覺得比一個人吃冷凍食品要好吃不知道多少陪。

兩人各添了一碗飯，小型的鍋子就乾乾淨淨。於是他們合力收拾善後，輪流去洗澡，一起坐在沙發上寫完功課，轉眼間時間就過了晚上九點。

春雪的平均就寢時間是十一點前後，但仁子打了個大大的呵欠。於是他站了起來，心想不如自己今天也早點睡。明天就是校慶當天，能睡飽當然再好不過。

「我家老媽到明天都不會回來，你可以睡她寢室。」

春雪這麼一說，穿著長T恤當睡衣的仁子就以摻雜著第二個呵欠聲的嗓音回答道：「好～～」乖乖走向春雪母親的寢室。兩人在走廊互道晚安，小小的背影消失在門後，春雪才鬆了一口氣。

春雪走向自己位於反方向的房間，用語音指令設定枕邊的鬧鐘後，整個人倒在床上，以前他睡覺時都會卸下神經連結裝置，但最近常常戴著睡。理由是儘管很少發生，但確實曾經有軍團伙伴在睡覺時打來。儘管對方多半都睡昏了頭而做出奇妙的反應，但春雪仍然想接這些通話。到了深夜時覺得孤單，想感受到與別人相連的感覺，這種心情春雪也很能體會。

──因此當春雪關掉房間的燈閉上眼睛，意識正要落入睡眠的深淵時還把聽見的聲音當成了線上的語音通話。

「喂，Crow你睡了喔？」

「啊……沒、沒有啦，我沒睡呵哈……」

「……那我有點事情要跟你說。」

「好，請說……」

春雪住黑暗中眨了眨眼，等著這段應該是透過網路進行的談話繼續進行，沒想到……

床在砰的一聲中突然晃動，讓春雪嚇得跳起了三公分左右。

「哇？夜夜夜燈，打開。」

他趕緊用語音指令點亮小夜燈，身體往左扭轉。眼前的身影無疑就是仁子。她用右手撐著頭側躺在床上，不知怎地還以不高興的表情瞪著春雪。

──難道不是語音呼叫，是全感覺呼叫？這是仁子的虛擬角色？

春雪以尚未完全清醒的腦袋想到這裡，就想伸出右手去摸摸看，但隨即注意到一陣清爽的香皂氣味。虛擬角色身上的確可以設定氣味，但這氣味顯然是來自有田家浴室常備的香皂，而仁子沒有理由特地重現這種氣味。也就是說……

「……是、是真貨？」

「那還用說？」

「……為、為什麼……啊，是這樣啊。不用怕啦，我媽的房間不會鬧鬼。」

「才不是！」

仁子先用左手在春雪肚子上打了一拳，然後才說：

「我說你喔，仔細想想最重要的事情你根本都沒講到吧？」

「最、最重要？你是說……」

在柔和的橘色間接照明燈光照亮下，春雪盯著仁子的臉看了好幾秒才總算想起是什麼事。就在幾個小時前，他們一起煮咖哩之前他的確說過：「這要講很久很久，我們邊吃邊說吧」。而這要講很久的事，就是關於利用神獸級公敵襲擊紅色軍團的神秘黑色虛擬角色。

「啊……啊，對喔！我都忘了。」

春雪從床上坐起，本能地換成跪坐姿勢低頭道歉：

「抱、抱歉，我忘了！我不是想蒙混過去，只是怎麼說，沒想到咖哩那麼好吃，就只顧著吃，其他的事都忘了……我當然會告訴你。那，呃，我們先到客廳……」

「麻煩死了，在這裡說就好了啦。」

春雪正要起身，仁子打斷他的話頭，翻成仰臥姿勢閉上眼睛。

春雪糾結地心想在這裡是要怎麼說，最後還是無可奈何，只好重新坐好。仔細想想，五天前他們就在這張床上一起睡過。雖然不是說睡過一次就可以睡第二次，但既然是紅之王的旨意，總覺得好像也就非聽不可……

「只是我雖然叫你說，其實我自己也多少猜到了一點。」

仁子忽然輕聲說出這樣的話，讓春雪中斷思考往身旁看了一眼。

仁子讓一頭解開了絲帶的紅頭髮，灑落在不知道什麼時候被搶去的枕頭上。雙眼看著天花板，輕輕動著嘴唇：

「我說的不是那個假Lotus的身分，而是他的目的。他多半不是想妨礙我們軍團獵公敵，也不是想獵殺我。他的目的多半……是想看清楚。」

「看、看清楚……？你是說……觀察？」

「沒錯。說得更精準一點，是要評占戰力……評估日珥的主力成員，以及我Scarlet Rain的戰力……」

說出這句話的瞬間，仁子──第二代紅之王臉上，露出了從今天出現在電梯間以來最為嚴肅的表情。春雪倒抽一口氣，小聲問個清楚：

「可、可是仁子，你們不是說那個黑色虛擬角色在戰鬥開始前，就鑽進地面消失了嗎……？」

「對，他人是消失了。可是多半不是逃走，而是躲到附近觀察我們和公敵打的情形。該死，要是一開始就發現他的目的，我就不會動用那招了……」

「動用哪招……？是、是哪招？」

「就是我的強化外裝……『無敵號（Invincible）』以前都保留不用的能力。」

聽懂這句話的意思時，春雪壓抑不住身體的顫抖。

第二代紅之王「不動要塞」的綽號，當然就是來自她的武裝貨櫃群全部架設完成時的巨大模樣。專有名詞「無敵號」，指的就是仁子每次升級時都階段性持續取得的強化外裝集合體，同時也被評為當今加速世界最強大的遠程火力。春雪也曾經和要塞模式下的Rain對戰或並肩作戰過幾次，對那壓倒性的威力體會得極為深切。

但純就仁子這句話來看，似乎就連那些外裝全部架設的狀態，也還不是她的全力……她還保留了實力。

「保留的……能力……是怎樣的能力……？」

春雪不由得探出上半身這麼問。

「誰會告訴你啊？白痴。」

仁子答得理所當然，但立刻又露出淺淺的笑容說下去：

「我是很想這麼說啦，只是這件事跟你也有一點關連……你還記得吧？半年前災禍之鎧事件的那次，我和你、Lotus還有博士一起去池袋，結果不就中了那個香蕉男的埋伏？當時我雖然叫出了外裝，結果卻被那些黃色的傢伙貼上來。那時候可真是出醜啊……」

「可、可是那有什麼辦法？對方人數那麼多，無論如何就是會被接近……」

「這個世界沒有簡單到講一句沒辦法就能了事，這你應該也已經知道了好不好……然後我也跑去山上修行了一下，說來這還是跟你看齊啊。」

「修行……怎、怎樣的修行……？」

「提示時間結束……不管怎麼說，昨天遇到那個大得該死的神獸級公敵，我為了掩護團員逃走就動用了這張底牌，結果被他看到了……說不定還被錄影下來。」

仁子說到這裡先閉上嘴又翻了個身，從正面看著春雪。

「我要說的都說完了，這次換你說給我聽了……告訴我那個像影子的假Lotus到底是什麼人。」

「……嗯，知道了。雖然這只是我的猜測……」

春雪點點頭，決定先解除跪坐模式。但他的雙腳已經輕微發麻，一不小心就倒在床上。他本想重新坐起，但看來仁子根本不在意他的姿勢，春雪也就繼續躺在床上，深深吸一口氣說出了他的名字。

「仁子看到的黑色虛擬角色，名稱是──『Black Vice』。他自稱是現在在加速世界裡散播ISS套件的集團『加速研究社』的副社長。」

「……Black Vice……」

仁子小聲復誦，春雪從極近距離凝視她的表情。

春雪過去之所以不對仁子與Pard小姐提起Vice的名字，自有他的理由。因為他害怕……因為他討厭。他不希望別人覺得同樣冠有「Black」色名的那個積層型虛擬角色，和他所敬愛的黑之王有任何一點關連。

春雪承受不了沉默，自己主動開了口：

「仁子，你知不知道BRAIN BURST里，曾經發生過不只一個虛擬角色有著同種顏色的情形……？」

「……據我所知，從來不曾發生過『撞色』的情形。Crow，這Black Vice的名字，你是從體力計量表或對戰名單上看到的嗎？」

「呃、呃呃……」

聽她這麼一說，春雪在腦中依序檢查過去幾次遭遇的場面搖了搖頭。

「……不是。基本上每次碰到Vice，都是在無限制空間……唯一的例外，就是在赫密斯之索縱貫賽那次。可是當時他一出現又馬上消失，所以沒顯示計量表……我和黑暗星雲的團員，都不曾在系統顯示的資訊中看到Vice的名字。」

「哼，原來如此啊。那麼這Black Vice的名字……也有可能不是真正的虛擬角色名稱，只是自稱而已。」

「咦咦咦！也就是說他擅自稱自己是Black……自稱是『純色的黑』了？」

「如果這是事實，就已經不只是自稱甚至是僭越了。」

仁子輕輕啐了一聲，接著將銳利的視線投向春雪身後。春雪順著她的視線望去，看到從窗戶射進的蒼白月光，在內嵌式書櫃上照出了複雜的影子。春雪拉回視線，繼續說明：

「……Black Vice能讓構成他身體的薄板自由變形，形成其他虛擬角色的剪影。我自己就曾經差點被假裝成學姊的他給騙了。還有……他還可以潛進場地上的影子裡，在裡面自由移動。所以仁子你們看到的『從公敵背上跳下來鑽進地面』，其實……」

「不是在場地上打出大洞，只是躲進影子裡……是吧？」

「嗯。所以，我想他確實很有可能順著影子移動，躲在附近觀察仁子戰鬥的情形。再補充一點，他能利用大腦內建式晶片的力量，任意讓知覺減速……我想就是因為這樣，他才能在無限制空間裡對你們設下埋伏。」

「這樣……啊。原來……是加速研究社的人啊……」

仁子點點頭，慢慢放鬆全身的力道，翻身改成仰臥姿勢。過了幾秒鐘後她輕聲細語地開始說道：

「……剛剛說到撞色。我……就曾經想過幾次，想說這個世界裡有沒有物品可以改變顏色。」

「改、改變……顏色……？」

「我也不是說玩膩了遠程攻擊所以想變成藍色，或是想玩支援所以要變成黃色。只是想到……如果我的顏色可以再濃一點就好了。從腥紅色（Scarlet）……變成純粹的紅色（Red）。」

「仁……仁子……？」

春雪震驚過度，只能呼喊她的名字。

系統賦予對戰虛擬角色的顏色，並不是隨機決定。是由BB程式讀取大腦深層的心象，以一種顏色來表現。因此從某個角度來看，色名對超頻連線者來說就與本名無異。

仁子說出了這種等於是否定自己「心靈色」的話，在床上縮著身體低頭不語。她嘴唇連連發顫，吐露出更加細微的聲音。

「……半年前，黃之王Yellow Radio不惜解放災禍之鎧也要引我出來，想利用9級的一戰定生死規則把我逐出加速世界。你想他的理由……他的動機，是什麼……？」

「那……當然是為了讓他自己升上10級……」

「多半不是。不管他嘴上怎麼說，心裡根本對10級一點興趣也沒有。他只是覺得我礙眼所以想獵殺我。就只是這樣而已。」

「可是……紅色軍團（日珥）和黃色軍團（宇宙秘境馬戲團）的領土，分別在東京的西邊跟東邊，根本沒有相鄰……」

「他覺得礙眼的不是日珥，是我個人。Radio他……就是沒辦法忍受不是紅色，而是腥紅色的我，自稱是純色之王。而我想其他的幾個王，或多或少也都這麼想……」

「才、才不會！」

春雪微微昂起上身，拚命搖頭。

「學姊她絕對，絕對不會……」

仁子見狀微微苦笑，左手指尖碰在春雪胸口安撫他。

「對，Lotus例外。畢竟她根本就想把那些王全都宰了。她真的是個不得了的人物，真的好厲害……」

仁子的嗓音即將消失之際，發出一陣深深的顫抖。她小小的手輕輕按在春雪右胸，抓住他當睡衣的T恤。

「……我會繼承上一代紅之王成為日珥的首領，有一半是被趕鴨子上架……可是我沒有後悔。軍團裡的人都很好，而且也多虧這樣，我才會認識Pard。所以我想保護現在的日珥……保護練馬戰區。可是……可是……」

仁子說到這裡，從床單上挪動身體，把額頭用力靠在春雪胸口。

「……連我自己都已經知道。我……我很脆弱！」

仁子嘔血似的聲調，讓春雪有種心臟被射穿似的感覺。他拚命伸出手，抱住仁子纖細的左肩說：

「才、才不會，仁子明明那麼強。不然怎麼可能升得上9級……」

「就是因為升上了9級，所以我才更清楚。不只是Originator的藍之王（Knight）和綠之王（Grandee），像紫之王（Thorn）、黃之王（Radio），還有黑之王（Lotus）也是一樣。要是認真地單挑，憑我……是贏不了的……」

仁子拾起頭來將濕潤的雙眼朝向春雪，以又哭又笑的表情說下去：

「……我就告訴你吧。半年前，Cherry他被災禍之鎧上身時，我用強硬手段查出你的現實身分，混進了這個家。這雖然是為了藉助你的飛行能力來捉住災禍之鎧……可是，其實我的目的不是只有這樣。我……打從心底害怕復活的黑色軍團，害怕回歸加速世界的黑之王Black Lotus。我想說她最先進攻的目標一定是練馬……想到說要是黑之王親自殺過來，我一定贏不了她……所以……我就打算先查出等級還低的Silver Crow的身分……先當作保險，免得被你們攻擊。我打的就是這種卑鄙、狡猾的主意……」

大滴的眼淚終於從睜大的眼睛落下，但仁子仍然在嘴邊留住自嘲的笑容，以細微的嗓音說下去：

「結果……結果，不管是你還是Lotus，人都那麼好……都好好聽我說話，還讓我在這裡過夜……陪我一起吃飯、一起打電玩、一起睡。這讓我好開心、好放心，可是……可是我從那一天起，就一直對你和Lotus說謊。我假裝自己很強，用平起平坐的口氣跟她說話。可是我不是純色，是冒牌的紅色。要說僭越稱王，我也有一份！」

「才……才不是這樣，絕對不是！」

春雪右手繞上她纖細的背，用力將她擁入懷中大喊：

「仁子才不是冒牌貨！你明明就比誰都更堅強、更勇敢，把軍團帶得有聲有色！今天領土戰裡碰到的Blaze他們，也都那麼相信你，仰慕你！這樣的仁子，怎麼可能會是冒牌……」

「可是，憑我保護不了他們！就連Cherry……連我唯一的『上輩』，我都保護不了啊！」

仁子以嘔血似的聲音這麼呼喊，再度將額頭靠在春雪胸口。抓著T恤的小小拳頭上所灌注的力道，強得彷佛恨不得把自己捏碎。

「……我就是感覺得到。加速世界裡正要發生大事。一種比ISS套件、梅丹佐出現之類的事情更重大的事。六大軍團的互不侵犯條約，大概也不會永久持續下去。如果……如果Radio和其他王再次認真想毀了我和日珥……」

「就算……就算那樣，仁子也不會輸的！所以你才會努力練出比那麼天下無敵的『要塞模式』更強的力量，不是嗎？」

「要是9級玩家之間真心廝殺起來……到了最後關頭一定會演變成什麼規則都不管，變成不擇手段的心念戰。你也知道我用不出攻擊型的心念吧……創造出我這個對戰虛擬角色的，是恐懼。像刺蝟一樣用武裝的外殻包住自己，拚命讓自己遠離外界……這就是我的本質。無論怎麼用心念強化射程跟逃跑的本事，只憑這些……絕對贏不了他們的『破壞心念』……」

仁子以慢慢變弱的語氣說完，左手不再用力，無力地落到床單上。接著像是忍耐寒冷似的，雙手抱住膝蓋縮成一小團。

春雪滿心想說更多話鼓勵她，一張嘴卻僵住不動。仁子過去的確曾在春雪與拓武面前說過，說她只會用「強化射程」和「強化移動」這兩大類的心念，學不會「強化攻擊」與「強化防御」。

其實這點春雪也是一樣。目前已經學會的「雷射劍」和「雷射長槍」都屬於強化射程的心念，而「光速翼」則屬於強化移動的心念。他使不出任何一招強化攻擊力或防御力的心念。

儘管如此，春雪仍然深深吸一口氣對她說：

「……那，我來保護你。」

他右手碰著的背微微一顫。

「要是仁子遇到危險，我隨時都會飛去救你。我會用連Originator看了都會嚇到的破壞心念，管他是誰都一招轟掉。」

「……你幾時又會那種心念了？」

仁子的聲音細小得要像這樣緊貼在一起才聽得見，但仍然找回了幾分平常的嗆辣，讓春雪深深點頭：

「要比想法負面，誰也贏不了我！我有一招Giga Destroy的招式。別說是王了，甚至可以打爛整個對戰空間，讓戰鬥強制結束……只是這招我還沒開發出來……」

「我說你喔，那樣的話連我也會被牽連進去好不好？」

仁子說著慢慢放開縮起的身體，抬起頭來。

她眼睛紅腫，睫毛上也沾滿了許多水滴，嘴邊卻已經有著淡淡的笑容。她舉起左手，但這次是輕輕去捏春雪的臉頰：

「……你這傢伙真的很單純。如果我是假哭想騙你投靠日珥，你要怎麼辦啊？」

仁子說著嘻嘻一笑，有銀色的光點從她的雙頰流落。這寶石般美麗的水珠，不可能是假的眼淚。

但仁子也只有這一瞬間露出她這年紀該有的哭泣表情，接著她立刻放開春雪的臉頰，毫不猶豫地將兩人之間十五公分左右的距離拉近到零。光腳丫碰上春雪的腳，細嫩的額頭碰上他的左臉。

「咦、我、我說啊，呃。」

雖說這不是他們兩人第一次一起睡在這張床上，但以前從來不曾這麼接近，讓春雪不由得發出為時已晚的破嗓呼聲。但就在春雪更進一步說話或動作之前……

「……謝啦。」

這個聲音隨著嘴唇的動作傳了過來，深深透進春雪心中。腦子忽然轉為平靜，本要上升的心跳也穩定下來。沒過多久，一陣不可思議的平靜籠罩住春雪。

「……嗯、嗯。」

春雪發出連自己也弄不清楚是肯定還是否定的回應，下意識的放下右手，輕輕撫摸她的一頭紅髮。仁子更加放鬆了全身的力道，以非常自然的聲音輕聲說：

「畢竟這種事情，就算對Pard我也說不出口啊……不好意思啊，跟你說了這麼多有的沒的事情。」

「沒關係啦。陪你說話這種小事我隨時奉陪……啊，當然我說要保護你那句話可是真心話喔。」

「呵呵，我會認真期待。」

仁子混著笑聲這麼回答，一瞬間以眼珠朝上的眼神看了春雪一眼，隨即又把臉的方向拉回來說：

「……為了答謝你聽我說這麼多，我就順便告訴你吧。」

「咦……告訴我什麼？」

春雪微徽歪頭，就聽到一句令他意想不到的話。

「我、Pard，還有黑暗星雲的那些女人會在乎你的理由。」

「什……什麼？」

春雪忍不住連連眨眼，但坦白說他對這件事有點……不，是相當有興趣。因為直到現在，每次軍團裡的女性們對他好，他內心深處都還是會忍不住感到懷疑「為什麼要對我這種人這麼好」。

但仁子頓了一會兒後才說出的話，卻又出乎他意料之外。

「這用說的你可能也不懂……我們女性型超頻連線者待在現實世界的時候，總是會有一點點的『恐懼』。跟我剛才說的恐懼不太一樣。」

「恐……懼？是對什麼的恐懼……？」

「我想想……算是對其他人。說得更精確一點，大概是對現實世界的男性型感到恐懼吧。」

「咦……男性型？你是指男人？」

「對。在化身為加速世界的對戰虛擬角色時，我們都有堅硬的裝甲保護。就算是女性型，跟男性型打也一點都不吃虧。可是等到對戰結束，回到現實世界的瞬間，這種力量就會消失。當超頻連線者的時間愈長，愈是會感受到血肉之軀的自己是多麼無力……」

「……血肉之軀的……無力……」

就如仁子先前所說，現在的春雪的確很難體會她所要描述的感覺。當然春雪待在現實世界時，也曾經想像過如果能像Silver Crow那樣自由飛天該有多好，或許也曾想過希望變得像Crow那樣強悍。但那並不是迫切的渴望，純粹只是夢想……

──不對，不是這樣。春雪這輩子第一次加速的那個時候，是非常強烈地渴望。

當黑雪公主在梅鄉國中的交誼廳，將BB程式所具備的驚人性能告訴春雪時他最先問的問題就是：「只要懂得善用BRAIN BURST我打架是不是就打得贏？」當時春雪的確渴望能夠痛毆先前虐待他的人。就是因為痛切體會過血肉之軀的自己有多麼無力，才會有這樣的渴望。

要不是黑雪公主快速排除掉那群不良少年，也許春雪到現在還有著這樣的渴望。渴望在現實世界也像Crow那麼能打，又因為沒辦法實現而始終覺得恐懼……

當春雪想到這裡，總算重新認知到現在的狀況。他吞了吞口水，膽顫心驚地問：

「咦……那，仁子現在……對我，也有這種恐懼……？」

「我要說的就是這個啊。」

所幸仁子並未切換成「春雪大哥哥好可怕……」這種對他心臟不太好的模式，反而輕輕戳著春雪的臉頰輕聲說：

「……你完全不會讓我覺得恐懼。我想Pard、Lotus，還有Raker跟Maiden，一定也是這樣。這種特質其實很了不起……畢竟我就連在學校裡面，陰錯陽差跟同班男生獨處，都會覺得有點不安。就算腦袋知道他根本不可能對我怎麼樣，我還是會怕。」

「……就算有公共攝影機，還是會怕……？」

「對。無論如何，就是沒辦法不覺得害怕。害怕自己沒有裝甲保護……也沒有必殺技、沒有強化外裝，什麼都沒有。在加速世界累積的時間越長，這種恐懼也就會醞釀得越強大。也許將來有一天，只要待在現實世界就會二十四小時都擺脫不了這種恐懼……」

「這、這……」

春雪拚命尋找有什麼話可以多少降低仁子的恐懼。但他先前說了不知道多少次的「仁子很強」這句話，偏偏在這個時候派不上用場。正是因為在加速世界得到了幾乎最強的實力，在現實世界才更會覺得不安。

正當春雪反覆做著微微張嘴又閉上的動作……

仁子看著他這樣，莫名地開心嘻笑起來。

「沒關係啦，什麼都不用說。我剛才不是告訴過你，說只有你不會讓我害怕嗎？還不只是這樣，跟你……跟『春雪』在一起，累積在我心中的恐懼就是會一下子變得很小。跟你黏在一起，我就可以很放心。就好像凝結成塊的負面心念……被溫暖的光慢慢淨化……」

「……呃、呃……」

春雪已經不知道該做什麼反應才好，仁子則將剛才所說的話付諸實行，像要把全身都交給春雪似的靠過去說：

「黑暗星雲那些老資格的女玩家，一定也有一樣的感覺。前陣子開咖哩派對的時候，她們臉上的表情喔……真的是完全放下了戒心。給我笑得那麼開心，實在是……」

「……是、是這樣嗎……」

春雪自己完全沒有自覺，尤其對黑雪公主與楓子，更是只想得起自己被他們嚴格教導的場面，讓他不由得歪了歪頭。接著他就這麼想著仁子這番話的含意想了好一會兒，忽然皺起眉頭說：

「呃……仁子，你的意思也就是說，是因為我從外表就看得出是個絕對安全，人畜無害的角色……？」

如果真是這樣，身為一個現實中的男性型角色，心境不免有點複雜。春雪正想著這種不像他會想的念頭，仁子的雙手就伸了過來從左右夾住春雪的瞼。

春雪本以為會像平常那樣被她捏著臉頰往右右用力拉，但仁子卻維持這樣的姿勢以平靜的笑容對他說：

「才不是啦。我說的是你這張圓臉的裡面……你的心。就是因為知道春雪老～～是那樣拚命，全力為我們著想，待在你身邊才會覺得放心……別看我每次都凶你，其實我也在想……想說總有一天，一定要好好答謝你。」

「哪、哪裡……我也沒做什麼具體的……」

「不用啦，只要陪在身邊那就夠了。所以……你可不要變了個人。就算你等級升上去成了高等級玩家，你也要繼續當原本的你。這樣一來……哪怕我有一天……」

仁子說到這裡就不再說，把捧著春雪臉頰的雙手移到他的頸子上。接著臉也湊了過去，就像想聽春雪心跳聲似的貼上他胸口，微笑著閉上眼睛。

──明天早上醒來，仁子多半又會變同平常的仁子，變回那個別說眼淚，根本就不顯露出半點脆弱的絕對火力紅之王。

──可是，我不會忘記。我不會忘記仁子是紅之王的同時，也是個比我小兩歲的小女生，也不會忘記我曾經答應過要保護這樣的仁子。

春雪慢慢沉入水面緩緩上升的睡夢深淵，同時將這幾句話銘記在心。當他閉上眼睛，就聽到細微的呼吸聲。聽著聽著，思緒也開始慢慢擴散。就在意識即將中斷之際，春雪覺得聲音來源似乎微微移動，接著有東西碰在臉頰上，但他不知道那是不是夢。

# CONTENTS

把一億年按鈕按個不停  
一億年ボタンを連打した俺は、気付いたら最強になっていた～落第剣士の学院無双～  
一億年按鈕按個不停，回過神來我變成了最強～吊車尾劍士在學院開無雙～  

作者： 月島 秀一  



- :closed_book: [README.md](README.md) - 簡介與其他資料
- :pencil: [整合樣式](%E6%95%B4%E5%90%88%E6%A8%A3%E5%BC%8F.md)
- [含有原文的章節](ja.md) - 可能為未翻譯或者吞樓，等待圖轉文之類
- [待修正屏蔽字](%E5%BE%85%E4%BF%AE%E6%AD%A3%E5%B1%8F%E8%94%BD%E5%AD%97.md) - 需要有人協助將 `**` 內的字補上
- [格式與譯名整合樣式](https://github.com/bluelovers/node-novel/blob/master/lib/locales/%E6%8A%8A%E4%B8%80%E5%84%84%E5%B9%B4%E6%8C%89%E9%88%95%E6%8C%89%E5%80%8B%E4%B8%8D%E5%81%9C.ts) - 如果連結錯誤 請點[這裡](https://github.com/bluelovers/node-novel/blob/master/lib/locales/)
-  :heart: [EPUB](https://gitlab.com/demonovel/epub-txt/blob/master/syosetu/%E4%B8%80%E5%84%84%E5%B9%B4%E6%8C%89%E9%88%95%E6%8C%89%E5%80%8B%E4%B8%8D%E5%81%9C%EF%BC%8C%E5%9B%9E%E9%81%8E%E7%A5%9E%E4%BE%86%E6%88%91%E8%AE%8A%E6%88%90%E4%BA%86%E6%9C%80%E5%BC%B7%EF%BD%9E%E5%90%8A%E8%BB%8A%E5%B0%BE%E5%8A%8D%E5%A3%AB%E5%9C%A8%E5%AD%B8%E9%99%A2%E9%96%8B%E7%84%A1%E9%9B%99%EF%BD%9E.epub) :heart:  ／ [TXT](https://gitlab.com/demonovel/epub-txt/blob/master/syosetu/out/%E4%B8%80%E5%84%84%E5%B9%B4%E6%8C%89%E9%88%95%E6%8C%89%E5%80%8B%E4%B8%8D%E5%81%9C%EF%BC%8C%E5%9B%9E%E9%81%8E%E7%A5%9E%E4%BE%86%E6%88%91%E8%AE%8A%E6%88%90%E4%BA%86%E6%9C%80%E5%BC%B7.out.txt) - 如果連結錯誤 請點[這裡](https://gitlab.com/demonovel/epub-txt/blob/master/syosetu/)
- :mega: [https://discord.gg/MnXkpmX](https://discord.gg/MnXkpmX) - 報錯交流群，如果已經加入請點[這裡](https://discordapp.com/channels/467794087769014273/467794088285175809) 或 [Discord](https://discordapp.com/channels/@me)


![導航目錄](https://chart.apis.google.com/chart?cht=qr&chs=150x150&chl=https://gitlab.com/novel-group/txt-source/blob/master/syosetu_out/把一億年按鈕按個不停/導航目錄.md "導航目錄")




## [null](00000_null)

- [一億年按鈕和時間牢獄【一】](00000_null/00010_%E4%B8%80%E5%84%84%E5%B9%B4%E6%8C%89%E9%88%95%E5%92%8C%E6%99%82%E9%96%93%E7%89%A2%E7%8D%84%E3%80%90%E4%B8%80%E3%80%91.txt)
- [一億年按鈕和時間牢獄【二】](00000_null/00020_%E4%B8%80%E5%84%84%E5%B9%B4%E6%8C%89%E9%88%95%E5%92%8C%E6%99%82%E9%96%93%E7%89%A2%E7%8D%84%E3%80%90%E4%BA%8C%E3%80%91.txt)
- [落第剣士と剣術学院【一】](00000_null/00030_%E8%90%BD%E7%AC%AC%E5%89%A3%E5%A3%AB%E3%81%A8%E5%89%A3%E8%A1%93%E5%AD%A6%E9%99%A2%E3%80%90%E4%B8%80%E3%80%91.txt)
- [落第剣士と剣術学院【二】](00000_null/00040_%E8%90%BD%E7%AC%AC%E5%89%A3%E5%A3%AB%E3%81%A8%E5%89%A3%E8%A1%93%E5%AD%A6%E9%99%A2%E3%80%90%E4%BA%8C%E3%80%91.txt)
- [落第剣士と剣術学院【三】](00000_null/00050_%E8%90%BD%E7%AC%AC%E5%89%A3%E5%A3%AB%E3%81%A8%E5%89%A3%E8%A1%93%E5%AD%A6%E9%99%A2%E3%80%90%E4%B8%89%E3%80%91.txt)
- [落第剣士と剣術学院【四】](00000_null/00060_%E8%90%BD%E7%AC%AC%E5%89%A3%E5%A3%AB%E3%81%A8%E5%89%A3%E8%A1%93%E5%AD%A6%E9%99%A2%E3%80%90%E5%9B%9B%E3%80%91.txt)
- [黒白の王女と魂装【一】](00000_null/00070_%E9%BB%92%E7%99%BD%E3%81%AE%E7%8E%8B%E5%A5%B3%E3%81%A8%E9%AD%82%E8%A3%85%E3%80%90%E4%B8%80%E3%80%91.txt)
- [黒白の王女と魂装【二】](00000_null/00080_%E9%BB%92%E7%99%BD%E3%81%AE%E7%8E%8B%E5%A5%B3%E3%81%A8%E9%AD%82%E8%A3%85%E3%80%90%E4%BA%8C%E3%80%91.txt)
- [黒白の王女と魂装【三】](00000_null/00090_%E9%BB%92%E7%99%BD%E3%81%AE%E7%8E%8B%E5%A5%B3%E3%81%A8%E9%AD%82%E8%A3%85%E3%80%90%E4%B8%89%E3%80%91.txt)
- [黒白の王女と魂装【四】](00000_null/00100_%E9%BB%92%E7%99%BD%E3%81%AE%E7%8E%8B%E5%A5%B3%E3%81%A8%E9%AD%82%E8%A3%85%E3%80%90%E5%9B%9B%E3%80%91.txt)
- [千刃学院と大五聖祭【一】](00000_null/00110_%E5%8D%83%E5%88%83%E5%AD%A6%E9%99%A2%E3%81%A8%E5%A4%A7%E4%BA%94%E8%81%96%E7%A5%AD%E3%80%90%E4%B8%80%E3%80%91.txt)
- [千刃学院と大五聖祭【二】](00000_null/00120_%E5%8D%83%E5%88%83%E5%AD%A6%E9%99%A2%E3%81%A8%E5%A4%A7%E4%BA%94%E8%81%96%E7%A5%AD%E3%80%90%E4%BA%8C%E3%80%91.txt)
- [千刃学院と大五聖祭【三】](00000_null/00130_%E5%8D%83%E5%88%83%E5%AD%A6%E9%99%A2%E3%81%A8%E5%A4%A7%E4%BA%94%E8%81%96%E7%A5%AD%E3%80%90%E4%B8%89%E3%80%91.txt)
- [千刃学院と大五聖祭【四】](00000_null/00140_%E5%8D%83%E5%88%83%E5%AD%A6%E9%99%A2%E3%81%A8%E5%A4%A7%E4%BA%94%E8%81%96%E7%A5%AD%E3%80%90%E5%9B%9B%E3%80%91.txt)
- [千刃学院と大五聖祭【五】](00000_null/00150_%E5%8D%83%E5%88%83%E5%AD%A6%E9%99%A2%E3%81%A8%E5%A4%A7%E4%BA%94%E8%81%96%E7%A5%AD%E3%80%90%E4%BA%94%E3%80%91.txt)
- [千刃学院と大五聖祭【六】](00000_null/00160_%E5%8D%83%E5%88%83%E5%AD%A6%E9%99%A2%E3%81%A8%E5%A4%A7%E4%BA%94%E8%81%96%E7%A5%AD%E3%80%90%E5%85%AD%E3%80%91.txt)
- [千刃学院と大五聖祭【七】](00000_null/00170_%E5%8D%83%E5%88%83%E5%AD%A6%E9%99%A2%E3%81%A8%E5%A4%A7%E4%BA%94%E8%81%96%E7%A5%AD%E3%80%90%E4%B8%83%E3%80%91.txt)
- [魔剣士と黒の組織【一】](00000_null/00180_%E9%AD%94%E5%89%A3%E5%A3%AB%E3%81%A8%E9%BB%92%E3%81%AE%E7%B5%84%E7%B9%94%E3%80%90%E4%B8%80%E3%80%91.txt)
- [魔剣士と黒の組織【二】](00000_null/00190_%E9%AD%94%E5%89%A3%E5%A3%AB%E3%81%A8%E9%BB%92%E3%81%AE%E7%B5%84%E7%B9%94%E3%80%90%E4%BA%8C%E3%80%91.txt)
- [魔剣士と黒の組織【三】](00000_null/00200_%E9%AD%94%E5%89%A3%E5%A3%AB%E3%81%A8%E9%BB%92%E3%81%AE%E7%B5%84%E7%B9%94%E3%80%90%E4%B8%89%E3%80%91.txt)


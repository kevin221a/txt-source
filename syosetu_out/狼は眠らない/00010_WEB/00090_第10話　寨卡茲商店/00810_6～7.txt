６

柯古陸斯是個大城鎮。跟沃卡城相當，說不定還更大一點。入口的城門莊嚴，外壁堅固。
車夫基多給門衛看了個什麼後，連稅金也沒付，就直接進入了。

很熱鬧。
商店也很多。
行進了一會後，馬車停下了。

「雷肯先生。契約在現在完成了。護衛任務就此結束。非常感謝。」
「寨卡茲商店，是在哪裡？」
「全部都是喔。」
「全部？」
「這座城鎮裡所有的店，都是寨卡茲商店。」
「喔喔。」
「本店的話，是在更裡面的地方。接下來要支付報酬，請跟我來。」
「知道了。」

接著，馬車進入了林立著富裕宅邸的區域，並向更深處前進。

然後，停在一間完全不像有在經營商賣的宅邸前方。
建築構造不是平民房屋。是貴族宅邸的構造。

基多給門衛看了個什麼後，門衛打開了緊閉的門。
正門旁有通常使用的門，有人進出。似乎是經常會有人出入的宅邸。

在等候室等了一陣子後，被帶領到當主的房間。
然後，雷肯、妮姫、艾達三人，與札克・寨卡茲見面了。


７

「坐下吧。你就是雷肯嗎。」
「對。」
「老夫是札克・寨卡茲。能介紹一下另外兩人嗎。」

札克相當年老，瘦能見骨，有著讓人想到猛禽的風貌。是個威風堂堂的老人。

「坐在我右邊的是妮姫，左邊的是艾達。」
「呼嗯。〈彗星斬擊之妮姫〉、〈千擊之艾達〉、然後是〈魔王雷肯〉嗎。護衛還真是豪華阿。」
「實在萬分抱歉。」
「無妨。這次的行李，有著那等價值。而且，也正好想見見雷肯。」

多波魯沒有出聲回應，將右手擺到胸前輕輕行禮。

「那麼，首先不謝罪可不行。雷肯。妮姫。艾達。這一次，我底下的三位冒険者，諾茲、利茨和努梅斯，為了稀少的武具而襲擊了你們。對於此事將予以道歉。喂。」

在左後方待機的看似執事的人物，接受札克的指示，將三個小袋子放到雷肯、妮姫和艾達面前。

「裡面各放了三枚金幣。一枚是這次委託的報酬。一枚是為這次的失態賠罪。最後一枚，是從三人的財產換來的錢。」

通常，打倒襲擊者的場合，襲擊者的財產會屬於打倒的人。這次，是依據多波魯的提議，之後再進行精算分配。

「金，金幣三枚。」

艾達很驚訝。

「這次的事，就以此收尾吧。把茶端來。」
「是。」

執事靜靜地走近門，向門外出聲後，四位女僕入室，在雷肯、妮姫、艾達、札克面前放了茶和點心，然後退室。

「喝吧。」
「〈鑑定〉。」

雷肯突然鑑定了茶。

「這還真是驚人。能使用〈鑑定〉嗎。而且連準備詠唱都沒有。雷肯，你到底是什麼人？」
「我就是我。」
「原來如此。那麼，〈鑑定〉後，得知了茶的好壊嗎？」
「得知了是從叫做奇姆尼的地方採來的茶葉，但不論怎樣，茶是好是壊什麼的我根本不知道。」
「喔？那麼，〈鑑定〉之後，知道了什麼？」

雷肯喝了一口茶，回答問題。

「知道了裡面沒有毒。」

這等同於向本人宣言，札克被當作是會對客人下毒的人。多波魯和執事的臉色瞬間變得嚴厲，但札克自己的反應不同。

「呼呼。呼呼呼呼。哈哈哈。哈哈哈哈。這還真是。被逗笑了呢。原來如此。這就是你的自我介紹嗎。」

妮姫靜靜地喝著茶，吃著點心。

艾達已經把點心吃完，正偷偷向雷肯的點心送出視線。

「因為你的來訪，讓老夫在沃卡的計畫全被打亂了。」

雷肯沒有回應，又喝了一口茶。

「這位多波魯雖然在遠方工作，但被叫了回來善後沃卡的事。事情的大致經過雖然有告訴他，但是呢，沒有說雷肯你的事。」

雷肯保持著沉默。
艾達向雷肯的點心伸出手。

「被傑尼商店僱用，護衛了前往邦塔羅伊的馬車的，是你們三個對吧。」
「啊啊。」
「在那不久之前，同樣是傑尼商店的馬車，有情報顯示，受到流浪的冒険者護衛。」
「喔。」
「似乎是個穿著黑衣的大男，有著不得了的使劍實力，動作還迅速得驚人。」
「到底是從哪弄到那種情報的。」
「呼嗯。這個質問的答案，要收一枚金幣。」
「那，不用了。」
「呼呼。那輛馬車上名叫艾文的車夫，好像是間諜的樣子。似乎被看穿了真面目，右手被砍斷，然後被逮捕了的樣子。」
「喔。」
「這也是那個黑色大男的傑作吧。」
「可能吧。」
「信用敵人的間諜，還長年重用，傑尼實在太讓人啞然了。這樣子連店裡的秘密被洩漏了多少，都無從得知。」
「原來如此。」
「艾文這男人，雖然不知道是從哪派來的，但為了主人，拚命地完成任務。這樣的男人，落到只得一死的地步，實在讓人感到遺憾。」
「還沒死吧。」

雷肯在應答的同時，一直注意著多波魯。在對話途中，多波魯的表情一瞬變得僵硬，放出了銳利的殺氣，但下一瞬間臉色又取回了平靜。但雷肯從中得知了想知道的事。

雷肯喝光了剩下不多的茶。

「那麼，想說的話，就這些嗎。那我們就回去了。」
「別急。還有些話想說。會再準備茶的。」

雷肯看了看眼前的點心盤。空的。被艾達吃光了。

「也能再來些點心嗎。」
「那當然了。」